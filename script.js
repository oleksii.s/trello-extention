const selectors = {
    comment: ".phenom.mod-comment-type",
    commentContent: ".current-comment",
    reactions: ".phenom-reactions .js-hide-on-sending",
    date: ".phenom-date .date",
    button: ".trello-copy-extension",
    buttonWrapper: ".trello-copy-extension-wrapper",
    buttonTooltip: ".trello-copy-extension-tooltip",
    mentions: ".atMention",
    smartLinks: ".atlaskit-smart-link, .known-service-link",
    reactRoot: ".js-react-root",
    deleteComment: ".js-confirm"
};

const classes = {
    showAll: "js-show-all-actions",
    commentContent: "current-comment",
    copyElement: "trello-copy-extension",
    copyElementWrapper: "trello-copy-extension-wrapper",
    copyElementTooltip: "trello-copy-extension-tooltip",
    active: "active"
};

setTimeout(init, 2000);
initMutationObserver();
document.addEventListener("click", clickHandler);

function initMutationObserver() {
    const config = { attributes: true, childList: false, subtree: true };
    let previousUrl = '';
    const callback = function(mutationsList) {
        for(const mutation of mutationsList) {
            if (location.href !== previousUrl && location.href.includes("https://trello.com/c/") || mutation.target.classList.contains("js-show-all-actions") || mutation.target.classList.contains("current-comment") || mutation.target.classList.contains("pop-over") || mutation.target.classList.contains("comment-box-input")) {
                previousUrl = location.href;
                setTimeout(init,2000)
            }
        }
    };

    const observer = new MutationObserver(callback);
    observer.observe(document.body, config);
}

function init() {
    const copyElementWrapper = document.createElement("span");
    const copyElement = document.createElement("span");
    const copyElementTooltip = document.createElement("span");
    copyElementWrapper.classList.add(classes.copyElementWrapper);
    copyElement.classList.add(classes.copyElement);
    copyElementTooltip.classList.add(classes.copyElementTooltip);
    copyElement.innerHTML = "Копировать";
    copyElementTooltip.dataset.succes = "Скопировано";
    copyElementTooltip.dataset.error = "Ошибка";
    copyElementTooltip.innerHTML = "Скопировано";
    copyElementWrapper.append(copyElementTooltip);
    copyElementWrapper.append(copyElement);


    const comments = [...document.querySelectorAll(selectors.comment)];

    comments.forEach(comment => {
        const reactions = comment.querySelector(selectors.reactions);

        if (!reactions) {
             return;
        }

        const dateElement = comment.querySelector(selectors.date);

        if (!dateElement) {
            return;
        }

        const date = dateElement.getAttribute("dt");

        if(comment.getAttribute("dt")) {
            return;
        }

        comment.setAttribute("dt", date);
        reactions.innerHTML = reactions.innerHTML + " - " + copyElementWrapper.outerHTML;
    });
}

function clickHandler(event) {
    const button = event.target.closest(selectors.button);

    if (button) {
        const buttonWrapper = button.closest(selectors.buttonWrapper);

        if (!buttonWrapper) {
            return;
        }

        const buttonTooltip = buttonWrapper.querySelector(selectors.buttonTooltip);

        if (!buttonTooltip) {
            return;
        }

        const comment = button.closest(selectors.comment);

        if (!comment) {
            return;
        }

        const commentContent = comment.querySelector(selectors.commentContent);

        if (!commentContent) {
            return;
        }

        try {
            const clipBoardText = HTMLtoMarkup(commentContent.cloneNode(true));
            navigator.clipboard.writeText(clipBoardText);

        } catch (error) {
            buttonTooltip.innerHTML = buttonTooltip.dataset.error;
            console.log(error)
        } finally {
            buttonTooltip.classList.add(classes.active);

            setTimeout(() => buttonTooltip.classList.remove(classes.active), 2000);
        }
    }
}

function HTMLtoMarkup(HTML) {
    const mentions = [...HTML.querySelectorAll(selectors.mentions)];

    mentions.forEach(mention => {
        mention.replaceWith(mention.innerHTML);
    });

    const smartLinks = [...HTML.querySelectorAll(selectors.smartLinks)];

    smartLinks.forEach(link => {
        link.replaceWith(link.href);
    });

    const links = [...HTML.querySelectorAll("a")];

    links.forEach(link => {
        if (link.href === link.innerText) {
            link.replaceWith(link.href);
        } else {
            link.replaceWith(`[${link.innerHTML}](${link.href})`);
        }
    });

    const uls = [...HTML.querySelectorAll("ul > li")];

    uls.forEach(item => {
        item.replaceWith(`- ${item.innerHTML}`);
    });

    const ols = [...HTML.querySelectorAll("ol")];

    ols.forEach(list => {
        [...list.children].forEach((item, index )=> {
            item.innerHTML = `${index+1}. ${item.innerHTML}`;
        });
    });

    const spans = [...HTML.querySelectorAll("span")];

    spans.forEach(span => {
        span.replaceWith(span.innerHTML);
    });

    const replaceCommands = {
        ["<"]: "&lt;",
        [">"]: "&gt;",
        ["&lt;p&gt;"]: "",
        ["&lt;/p&gt;"]: "\n",
        ["&lt;br&gt;"]: "\n",
        ["&lt;strong&gt;"]: "**",
        ["&lt;/strong&gt;"]: "**",
        ["&lt;pre&gt;&lt;code&gt;"]: "\n```\n",
        ["&lt;/code&gt;&lt;/pre&gt;"]: "\n```\n",
        ["&lt;em&gt;"]: "*",
        ["&lt;/em&gt;"]: "*",
        ["&lt;del&gt;"]: "~~",
        ["&lt;/del&gt;"]: "~~",
        ["&lt;hr&gt;"]: "\n---\n",
        ["&lt;h1&gt;"]: "#",
        ["&lt;/h1&gt;"]: "\n",
        ["&lt;h2&gt;"]: "##",
        ["&lt;/h2&gt;"]: "\n",
        ["&lt;h3&gt;"]: "###",
        ["&lt;/h3&gt;"]: "\n",
        ["&lt;h4&gt;"]: "####",
        ["&lt;/h4&gt;"]: "\n",
        ["&lt;h5&gt;"]: "#####",
        ["&lt;/h5&gt;"]: "\n",
        ["&lt;h6&gt;"]: "######",
        ["&lt;/h6&gt;"]: "\n",
        ["&lt;blockquote&gt;\n"]: ">",
        ["&lt;/blockquote&gt;"]: "\n",
        ["&lt;code&gt;"]: "`",
        ["&lt;/code&gt;"]: "`",
        ["&lt;ul&gt;\n"]: "",
        ["\n&lt;/ul&gt;"]: "",
        ["&lt;ol&gt;\n"]: "",
        ["\n&lt;/ol&gt;"]: "",
        ["&lt;li&gt;"]: "",
        ["&lt;/li&gt;"]: "",
        ["\n0."]: "\n0\\.",
        ["\n1."]: "\n1\\.",
        ["\n2."]: "\n2\\.",
        ["\n3."]: "\n3\\.",
        ["\n4."]: "\n4\\.",
        ["\n5."]: "\n5\\.",
        ["\n6."]: "\n6\\.",
        ["\n7."]: "\n7\\.",
        ["\n8."]: "\n8\\.",
        ["\n9."]: "\n9\\.",
        ["&lt;"]: "<",
        ["&gt;"]: ">",
        ["&amp;"]: "&"
    };

    const result = [...Object.entries(replaceCommands)]
        .reduce((result,entry) => {return result.replaceAll(entry[0], entry[1])}, HTML.innerHTML);

    return unescapeHTML(result);
}

function unescapeHTML(HTMLText) {
    return HTMLText.replaceAll('&lt;','<').replaceAll('&gt;','>').replaceAll('&amp;','&');
}

